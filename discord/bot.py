""" TBU Discord Bot  """
import psycopg2
import feedparser
from typing import Tuple, List, Dict
from helpers.sentry import client

# User imports
from settings import DATABASE, DISCORD
from discord import discord_api


def connect_db():
    conn = psycopg2.connect(
        host=DATABASE["POSTGRES"]["HOST"],    # your host, usually localhost
        user=DATABASE["POSTGRES"]["USER"],         # your username
        password=DATABASE["POSTGRES"]["PASSWORD"],  # your password
        database=DATABASE["POSTGRES"]["DB_NAME"]
    )
    cur = conn.cursor()
    return cur, conn


def article_is_not_db(cur, conn, table: str, article_title: str, article_url: str, article_date: str) -> bool:
    """ Check if a given pair of article title and date
    is in the database.
    Args:
        article_title (str): The title of an article
        article_date  (str): The publication date of an article
    Return:
        True if the article is not in the database
        False if the article is already present in the database
    """
    # print(table)
    cur.execute(
        """CREATE TABLE IF NOT EXISTS {table} (title TEXT, link TEXT, published TEXT)""".format(table=table))
    cur.execute("""SELECT * from {table}  WHERE title='{title}' AND link='{link}' AND published='{published}'""".format(
        table=table, title=article_title, link=article_url, published=article_date))
    if not cur.fetchall():
        return True
    else:
        return False


def add_article_to_db(cur, conn, table: str, article_title: str, article_url: str, article_date: str) -> None:
    """ Add a new article title and date to the database
    Args:
        article_title (str): The title of an article
        article_date (str): The publication date of an article
    """
    cur.execute("""INSERT INTO {table}(title,link,published) VALUES ('{title}','{link}','{published}')""".format(
        table=table, title=article_title, link=article_url, published=article_date))
    conn.commit()


def read_article_feed(cur, conn, FEED: str, table: str) -> List[str]:
    """ Get articles from RSS feed """
    posts = list()
    raw_feed = feedparser.parse(FEED)
    # Gets the lastest 20 feeds - if not it would download all posts.
    feed = raw_feed['entries'][0:20]
    for article in feed:
        if article_is_not_db(cur, conn, table, article['title'], article['link'], article['published']):
            posts.append(article['link'])
            add_article_to_db(
                cur, conn, table, article['title'],  article['link'], article['published'])
    return posts


def send(hook: str, msg: str) -> None:
    '''Post message into specific channel on the Discord Server.

    Arguments:
        webhooks {Dict} -- [This is a dictionary of all the Discord webhooks for the server.]
        channel {str} -- [Channel of Discord Server where you want to post the message]
        post {str} -- [Post Content Body]

    Returns:
        None
    '''

    hook = discord_api.Webhook(hook, msg=msg)
    return hook.post()


def run(log) -> None:
    '''This function starts the Discord Bot. It allows the user to control the runtime enviroment settings.

    Arguments:
        webhooks {Dict} -- [This is a dictionary of all the Discord webhooks for the server.]
        log {[logger]} -- [Python logger]

    Returns:
        None
    '''

    channels = set([
        "NEWS_COMICS",
        "NEWS_MOVIE",
        "NEWS_TV",
        "NEWS_VIDEOGAMES",
        "NEWS_MERCH",
        "NEWS_GENERAL",
        "PODCAST_COMICS",
    ])

    try:
        log.info("DB Connection Started")
        cur, conn = connect_db()
        log.info("DB Connection Successful")

        for channel in channels:
            log.info(f'Seeing if {channel} needs to be updated.')
            posts = read_article_feed(cur, conn,
                                      DISCORD[channel]["FEED"], DISCORD[channel]["TABLE"])
            if posts:
                log.info(f'{channel} needs to be updated.')
                # Sends all new posts from older to newer.
                posts = posts[::-1]
                for post in posts:
                    send(DISCORD[channel]["WEBHOOK"], post)
            else:
                log.info('No new posts')

    except Exception as err:
        log.critical(err)
        client.capture_exception()
