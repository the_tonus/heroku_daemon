"""Summary: Webscraper for comicbook.com
"""
import requests
from bs4 import BeautifulSoup
from typing import List, Tuple, Dict
from .functions import *


def raw_scrape(url: str, headers: Dict[str, str]):
    """Summary:
        Gets the raw data from site.

    Args:
        url (String): url for site
        headers (string): headers for bs4 to tell site it is a legitimate browser

    Returns:
        bs4: Raw scraped data
    """
    r = requests.get(url, headers=headers)
    c = r.content  # get html content

    soup = BeautifulSoup(c, "lxml")
    entries = soup.find_all('div', {'class': 'headline-box'})
    return entries


def extract_content(entries) -> Tuple[List[str], List[str]]:
    """Summary:
        Extracts titles and links from raw data
    Args:
        entries (bs4): Raw scraped data

    Returns:
        list: Scraped and cleaned article titles and links
    """

    titles = list()
    links = list()
    for entry in entries:
        headlines = entry.find_all('h1')
        for headline in headlines:
            articles = headline.find_all('a')
            for article in articles:
                titles.append(article.text)
                links.append(article['href'])

    titles = list(filter(None.__ne__, titles))
    links = list(filter(None.__ne__, links))
    return titles, links


def scraper() -> Dict[List[str], List[str]]:
    """Summary:
        Main webscraper function

    Returns:
        dict: dictionary output for Pandas DataFrame
    """
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0'}
    urls = [
        'http://comicbook.com/comics/news?Page={}'.format(i) for i in range(1, 4)]

    titles = list()
    links = list()

    for url in urls:
        entries = raw_scrape(url, headers)
        titles_temp, links_temp = extract_content(entries)
        titles = titles + titles_temp
        links = links + links_temp

    return dict_output(titles, links)


if __name__ == '__main__':
    print(scraper())
