"""Library of functions for Webscrapers
"""
import validators
from orderedset import OrderedSet
from webscrape.scrapers.search_criterion import criterion
from typing import List, Dict, Tuple

def search_content(titles: List[str], links: List[str]) -> Tuple[List[str], List[str]]:
    """Summary
        Filters each article title by search criterion and validates url.
        Uses OrderedSet to easily remove duplicates but maintain sequential order.
    Args:
        titles (List): List of article titles
        links (List): List of article links

    Returns:
        found_titles and found_links: Sorted and unique titles and links
    """
    found_titles = OrderedSet()
    found_links = OrderedSet()
    for key in criterion():
        for title, link in zip(titles, links):
            if (key.lower() in title.lower()) and (validators.url(link)):
                found_titles.add(title.strip())
                found_links.add(link.strip())
    return list(found_titles), list(found_links)


def json_output(titles: List[str], links: List[str]) -> Dict[str, str]:
    """Summary
        Return json output to generate api.

    Args:
        titles (List): List of article titles
        links (List): List of article links

    Returns:
        json: json api
    """
    found_titles, found_links = search_content(titles, links)
    data = dict()
    for i in range(len(found_titles)):
        data[i] = dict()
        data[i]['title'] = found_titles[i]
        data[i]['link'] = found_links[i]
    return data


def dict_output(titles: List[str], links: List[str]) -> Dict[List[str], List[str]]:
    """"Summary
        Return dictionary output for pandas DataFrame

    Args:
        titles (List): List of article titles
        links (List): List of article links

    Returns:
        dict: Dictionary of titles and links
    """
    found_titles, found_links = search_content(titles, links)
    data = dict()
    data['title'] = found_titles
    data['link'] = found_links
    return data
