"""Summary: Webscraper for nerdist.com
"""
import requests
from bs4 import BeautifulSoup
from typing import List, Tuple, Dict
from .functions import *


def raw_scrape(url: str, headers: Dict[str, str]) -> Tuple[List[str], List[str]]:
    """Summary:
        Gets the raw data from site.

    Args:
        url (String): url for site
        headers (string): headers for bs4 to tell site it is a legitimate browser

    Returns:
        bs4: Raw scraped data
    """
    r = requests.get(url, headers=headers)
    c = r.content  # get html content

    soup = BeautifulSoup(c, "lxml")
    raw_titles = soup.find_all('span', {'class': 'list-item-main-title'})
    raw_links = soup.find_all('a', {'class': 'post'})
    return raw_titles, raw_links


def extract_content(raw_titles: List[str], raw_links: List[str]) -> Tuple[List[str], List[str]]:
    """Summary:
        Extracts titles and links from raw data
    Args:
        entries (bs4): Raw scraped data

    Returns:
        list: Scraped and cleaned article titles and links
    """
    titles = list()
    links = list()
    for title, link in zip(raw_titles, raw_links):
        titles.append(title.text)
        links.append(link['href'])

    titles = list(filter(None.__ne__, titles))
    links = list(filter(None.__ne__, links))
    return titles, links


def scraper() -> Dict[List[str], List[str]]:
    """Summary:
        Main webscraper function

    Returns:
        dict: dictionary output for Pandas DataFrame
    """
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0'}
    url = 'https://nerdist.com/category/comics/'

    raw_titles, raw_links = raw_scrape(url, headers)
    titles, links = extract_content(raw_titles, raw_links)
    return dict_output(titles, links)


if __name__ == '__main__':
    print(scraper())
