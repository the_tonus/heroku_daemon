""" Main Webscrape Application
"""
import pandas as pd
from sqlalchemy import create_engine

# Webscrapers
from .scrapers import (bleedingcool,
                       cbr,
                       comicbook,
                       comicsbeat,
                       ign,
                       nerdist,
                       newsarama,
                       outhousers)

from helpers.sentry import client
from .sources import sources  # Webscraping Sources
from settings import DATABASE


def webscrape_functions(src: str):
    """Summary:
            This function is a 'switch' so that executes the webscraping functions for each source.

    Args:
            src (String): Webscraping Source

    Returns:
            Function: Webscraping function for specific source.
    """

    switcher = {
        'bleedingcool': lambda: bleedingcool.scraper(),
        'cbr': lambda: cbr.scraper(),
        'comicbook': lambda: comicbook.scraper(),
        'comicsbeat': lambda: comicsbeat.scraper(),
        'ign': lambda: ign.scraper(),
        'nerdist': lambda: nerdist.scraper(),
        'newsarama': lambda: newsarama.scraper(),
        'outhousers': lambda: outhousers.scraper(),
    }
    return switcher.get(src, lambda: "Invalid source")


def scraper(src: str, engine, log) -> None:
    """Summary
            Scrapes website according to source and uploads scraped data to database.
    Args:
            src (String): Webscraping Source
            engine (DB Engine): DB engine
            log (log): Logger
    """
    try:
        log.info(f'Beginning Scrape of {src}.')
        get_data = webscrape_functions(src)
        log.info(f'Ending Scrape of {src}.')

        df = pd.DataFrame(get_data())

        log.info(f'Uploading {src} data.')
        df.to_sql(src, con=engine, if_exists='replace')
        log.info(f'Uploading of {src} data completed.')
    except ValueError as e:
        log.critical(e)
        client.capture_exception()
        pass
    except Exception as e:
        log.critical(e)
        client.capture_exception()
        pass


def run(log) -> None:
    """Summary:
            Scrapes all the data for each source. Main function for webscraping service.

    Args:
            src (String): Webscraping Source
            log (log): Logger
    """
    # Set up database connection
    db_url = DATABASE["POSTGRES"]["URI"]
    engine = create_engine(db_url)
    log.info('Connection to db successful')

    # Where the magic happens! Scrapes via web source
    for src in sources:
        scraper(src, engine, log)

    log.info('Total webscraping complete. ')
